using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GidrosantehVue.Models;
using GidrosantehVue.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GidrosantehVue.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MainController : ControllerBase
    {
        ApplicationDbContext dbContext;
        RepairRequestManager repairRequestManager;
        public MainController(ApplicationDbContext dbContext, RepairRequestManager repairRequestManager)
        {
            this.dbContext = dbContext;
            this.repairRequestManager = repairRequestManager;
        }

        [Route("Categories")]
        [HttpGet]
        public IActionResult GetCategories()
        {
            var categories = dbContext.Categories
                .ToList();

            return Ok(categories);
        }

        [Route("CategoryInfo/{id}")]
        [HttpGet]
        public IActionResult GetCategoryInfo(int id)
        {
            var categoriesInfo = dbContext.CategoriesInfo
                .Where(x => x.CategoryId == id)
                .ToList();

            return Ok(categoriesInfo);
        }

        [Route("Send")]
        [HttpPost]
        public async Task<IActionResult> SendRequestCall([FromForm]RepairRequest repairRequest)
        {
            repairRequestManager.CreateRequest(repairRequest);
            return Ok();
            //if(!string.IsNullOrEmpty(phone))
            //{
            //    try
            //    {
            //        await new EmailService().SendEmailAsync("N.nabrodoff@mail.ru", "Заявка с Гидросанцентра", "Телефон - " + phone + " Имя - " + name);
            //    }
            //    catch
            //    {
            //        return BadRequest();
            //    }

            //    return Ok();
            //}
            //else
            //{
            //    return BadRequest();
            //}
        }
    }
}
