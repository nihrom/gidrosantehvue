using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Models
{
    public class DbSeed
    {
        public static async void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();

            if (!context.Categories.Any())
            {
                List<Category> categories = new List<Category>()
                {
                    new Category{  Name="РЕМОНТ ДУШЕВЫХ КАБИН", ImgUrl ="/images/1.jpg"},
                    new Category{ Name="РЕМОНТ И УСТАНОВКА УНИТАЗОВ/БИДЕ", ImgUrl ="/images/7.jpg"},
                    new Category{  Name="УСТАНОВКА И ЗАМЕНА РАКОВИН", ImgUrl ="/images/2.jpg"},
                    new Category{  Name="УСТАНОВКА И РЕМОНТ ВАНН", ImgUrl ="/images/3.jpg"},
                    new Category{  Name="РЕМОНТ И УСТАНОВКА СМЕСИТЕЛЕЙ", ImgUrl ="/images/4.jpg"},
                    new Category{  Name="УСТАНОВКА И РЕМОНТ ПОЛОТЕНЦЕСУШИТЕЛЕЙ", ImgUrl ="/images/5.jpg"},
                    new Category{  Name="ЗАМЕНА И УСТАНОВКА ФИЛЬТРОВ", ImgUrl ="/images/6.jpg"},
                    new Category{  Name="УСТАНОВКА БЫТОВОЙ ТЕХНИКИ", ImgUrl ="/images/stiral.jpg"},
                    new Category{ Name="РАЗВОДКА ТРУБ", ImgUrl ="/images/8.jpg"},
                    new Category{  Name="УСТАНОВКА, РЕМОНТ РАДИАТОРА", ImgUrl ="/images/9.jpg"}
                };

                context.Categories.AddRange(categories);
                context.SaveChanges();
            }

            if (!context.CategoriesInfo.Any())
            {
                List<CategoryInfo> categoriesInfo = new List<CategoryInfo>()
                {
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины", Price="4800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины", Price="2400"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины", Price="7200"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины с гидромассажем", Price="6800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины с гидромассажем", Price="3400"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины с гидромассажем", Price="10200"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины с парогенератором", Price="7800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины с парогенератором", Price="3900"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины с парогенератором", Price="11700"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины (гидромассаж + парогенератор)", Price="9800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины (гидромассаж + парогенератор)", Price="4900"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины (гидромассаж + парогенератор)", Price="14700"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины с низким поддоном", Price="6600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины с низким поддоном", Price="3300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины с низким поддоном", Price="9900"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевой кабины с высоким поддоном", Price="8600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевой кабины с высоким поддоном", Price="4300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевой кабины с высоким поддоном", Price="12900"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса", Price="8800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса", Price="4400"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса", Price="13200"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса с низким поддоном", Price="10600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса с низким поддоном", Price="5300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса с низким поддоном", Price="15900"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса с высоким поддоном", Price="12600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса с высоким поддоном", Price="6300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса с высоким поддоном", Price="18900"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса с гидромассажем", Price="10800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса с гидромассажем", Price="5400"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса с гидромассажем", Price="16200"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса с парогенератором", Price="11800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса с парогенератором", Price="5900"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса с парогенератором", Price="17700"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого бокса (гидромассаж + парогенератор)", Price="13800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого бокса (гидромассаж + парогенератор)", Price="6900"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого бокса (гидромассаж + парогенератор)", Price="20700"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого уголка", Price="4800"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого уголка", Price="2400"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого уголка", Price="7200"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого уголка с низким поддоном", Price="6600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого уголка с низким поддоном", Price="3300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого уголка с низким поддоном", Price="9900"},
                    new CategoryInfo(){CategoryId=0, Name="Установка душевого уголка с высоким поддоном", Price="8600"},
                    new CategoryInfo(){CategoryId=0, Name="Демонтаж душевого уголка с высоким поддоном", Price="4300"},
                    new CategoryInfo(){CategoryId=0, Name="Замена душевого уголка с высоким поддоном", Price="12900"},
                    new CategoryInfo(){CategoryId=0, Name="Монтаж пластиковых шторок для душевых кабин, боксов, уголков", Price="1800"},
                    new CategoryInfo(){CategoryId=0, Name="Монтаж стеклянных шторок для душевых кабин, боксов, уголков", Price="1800"},

                    new CategoryInfo(){CategoryId=1, Name="Установка унитаза/биде «под ключ» (не включая демонтаж старого)", Price="2400"},
                    new CategoryInfo(){CategoryId=1, Name="Демонтаж унитаза/биде", Price="1200"},
                    new CategoryInfo(){CategoryId=1, Name="Замена унитаза/биде", Price="3600"},
                    new CategoryInfo(){CategoryId=1, Name="Установка инсталляции (подвесного унитаза)", Price="3800"},
                    new CategoryInfo(){CategoryId=1, Name="Демонтаж инсталляции (подвесного унитаза)", Price="1900"},
                    new CategoryInfo(){CategoryId=1, Name="Замена инсталляции (подвесного унитаза)", Price="5700"},
                    new CategoryInfo(){CategoryId=1, Name="Установка писсуара", Price="2200"},
                    new CategoryInfo(){CategoryId=1, Name="Демонтаж писсуара", Price="1100"},
                    new CategoryInfo(){CategoryId=1, Name="Замена писсуара", Price="3300"},
                    new CategoryInfo(){CategoryId=1, Name="Установка сливного бачка", Price="800"},
                    new CategoryInfo(){CategoryId=1, Name="Демонтаж старого сливного бачка", Price="400"},
                    new CategoryInfo(){CategoryId=1, Name="Замена сливного бачка", Price="1200"},
                    new CategoryInfo(){CategoryId=1, Name="Ремонт сливного бачка (запорной арматуры)", Price="700"},
                    new CategoryInfo(){CategoryId=1, Name="Установка гофры/сальника", Price="500"},

                    new CategoryInfo(){CategoryId=2, Name="Установка и подключение раковины и мойки", Price="1800"},
                    new CategoryInfo(){CategoryId=2, Name="Установка раковины на кронштейнах", Price="900"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж раковины на кронштейнах", Price="2700"},
                    new CategoryInfo(){CategoryId=2, Name="Замена раковины на кронштейнах", Price="2200"},
                    new CategoryInfo(){CategoryId=2, Name="Установка раковины «Тюльпан»", Price="1100"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж раковины «Тюльпан»", Price="3300"},
                    new CategoryInfo(){CategoryId=2, Name="Замена раковины «Тюльпан»", Price="2600"},
                    new CategoryInfo(){CategoryId=2, Name="Установка накладной раковины", Price="1300"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж накладной раковины", Price="3900"},
                    new CategoryInfo(){CategoryId=2, Name="Замена накладной раковины", Price="2400"},
                    new CategoryInfo(){CategoryId=2, Name="Установка «Мойдодыра»", Price="2400"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж «Мойдодыра»", Price="1200"},
                    new CategoryInfo(){CategoryId=2, Name="Замена «Мойдодыра»", Price="3600"},
                    new CategoryInfo(){CategoryId=2, Name="Установка мойки", Price="1400"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж мойки", Price="700"},
                    new CategoryInfo(){CategoryId=2, Name="Замена мойки", Price="2100"},
                    new CategoryInfo(){CategoryId=2, Name="Установка мойки из нержавейки", Price="1600"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж мойки из нержавейки", Price="800"},
                    new CategoryInfo(){CategoryId=2, Name="Замена мойки из нержавейки", Price="2400"},
                    new CategoryInfo(){CategoryId=2, Name="Установка мойки из искусственного камня", Price="3200"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж мойки из искусственного камня", Price="1600"},
                    new CategoryInfo(){CategoryId=2, Name="Замена мойки из искусственного камня", Price="4800"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж старой раковины или мойки других брендов", Price="900"},
                    new CategoryInfo(){CategoryId=2, Name="Сборка тумбы под мойку", Price="1200"},
                    new CategoryInfo(){CategoryId=2, Name="Установка тумбы под мойку", Price="800"},
                    new CategoryInfo(){CategoryId=2, Name="Сверление отверстий в металлической мойке под смеситель", Price="400"},
                    new CategoryInfo(){CategoryId=2, Name="Врезка мойки в столешницу (в зависимости от материала)", Price="1900"},
                    new CategoryInfo(){CategoryId=2, Name="Установка сифона слива", Price="500"},
                    new CategoryInfo(){CategoryId=2, Name="Демонтаж старого сифона слива", Price="200"},
                    new CategoryInfo(){CategoryId=2, Name="Герметизация раковины и мойки", Price="700"},

                    new CategoryInfo(){CategoryId=3, Name="Установка чугунной ванны", Price="4200"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж чугунной ванны", Price="2100"},
                    new CategoryInfo(){CategoryId=3, Name="Замена чугунной ванны", Price="6300"},
                    new CategoryInfo(){CategoryId=3, Name="Установка стальной ванны", Price="2400"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж стальной ванны", Price="1200"},
                    new CategoryInfo(){CategoryId=3, Name="Замена стальной ванны", Price="3600"},
                    new CategoryInfo(){CategoryId=3, Name="Установка акриловой ванны", Price="2800"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж акриловой ванны", Price="1400"},
                    new CategoryInfo(){CategoryId=3, Name="Замена акриловой ванны", Price="4200"},
                    new CategoryInfo(){CategoryId=3, Name="Установка угловой ванны", Price="4800"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж угловой ванны", Price="2400"},
                    new CategoryInfo(){CategoryId=3, Name="Замена угловой ванны", Price="7200"},
                    new CategoryInfo(){CategoryId=3, Name="Установка гидромассажной ванны", Price="5800"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж гидромассажной ванны", Price="2900"},
                    new CategoryInfo(){CategoryId=3, Name="Замена гидромассажной ванны", Price="8700"},
                    new CategoryInfo(){CategoryId=3, Name="Установка сифона ванны", Price="1000"},
                    new CategoryInfo(){CategoryId=3, Name="Демонтаж сифона ванны", Price="500"},
                    new CategoryInfo(){CategoryId=3, Name="Вынос старой ванны", Price="по дог-ти"},

                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя простого на жёсткой подводке", Price="1400"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя простого на жёсткой подводке", Price="700"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя простого на жёсткой подводке", Price="2100"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя простого на гибкой подводке", Price="1200"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя простого на гибкой подводке", Price="600"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя простого на гибкой подводке", Price="1800"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя с душем", Price="1600"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя с душем", Price="800"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя с душем", Price="2400"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя скрытого монтажа (с скрытием коммуникаций в стене)", Price="2800"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя скрытого монтажа (с скрытием коммуникаций в стене)", Price="1400"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя скрытого монтажа (с скрытием коммуникаций в стене)", Price="4200"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя с термостатом", Price="2400"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя с термостатом", Price="1200"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя с термостатом", Price="3600"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя сенсорного", Price="2200"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя сенсорного", Price="1100"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя сенсорного", Price="3300"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя встраиваемого (на борт ванной, в том числе с душем)", Price="2600"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя встраиваемого (на борт ванной, в том числе с душем)", Price="1300"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя встраиваемого (на борт ванной, в том числе с душем)", Price="3900"},
                    new CategoryInfo(){CategoryId=4, Name="Установка смесителя врезного (с сверлением под смеситель в зависимости от материла у основания в месте установки)", Price="1800"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя врезного", Price="900"},
                    new CategoryInfo(){CategoryId=4, Name="Замена смесителя врезного", Price="2700"},
                    new CategoryInfo(){CategoryId=4, Name="Установка планки под монтаж смесителя", Price="900"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж смесителя других классификаций", Price="600"},
                    new CategoryInfo(){CategoryId=4, Name="Установка душа на готовую подводку (в том числе гигиенического)", Price="800"},
                    new CategoryInfo(){CategoryId=4, Name="Демонтаж душа на готовой подводке (в том числе гигиенического)", Price="400"},
                    new CategoryInfo(){CategoryId=4, Name="Замена душа на готовую подводку (в том числе гигиенического)", Price="1200"},
                    new CategoryInfo(){CategoryId=4, Name="Установка штанги для лейки душа", Price="400"},

                    new CategoryInfo(){CategoryId=5, Name="Установка полотенцесушителя ", Price="2000"},

                    new CategoryInfo(){CategoryId=6, Name="Установка фильтра ", Price="3000"},

                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины LG (без электрики)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины LG (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины BEKO (без электрики)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины BEKO (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины Bosh (без электрики)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины Bosh (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины Zanussi (без электрики)", Price="1300"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины Zanussi (без электрики)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины Ariston (без электрики)", Price="1300"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины Ariston (без электрики)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины Indesit (без электрики)", Price="1100"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины Indesit (без электрики)", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины Ardo (без электрики)", Price="1100"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины Ardo (без электрики)", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Установка стиральной машины в колонну (без электрики)", Price="1600"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины в колонне (без электрики)", Price="800"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж стиральной машины других брендов", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Герметизация стыков стиральной машины", Price="450"},
                    new CategoryInfo(){CategoryId=7, Name="Подключение слива стиральной машины (с герметизацией)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Подключение слива стиральной машины (с герметизацией)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины BEKO (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Bosh (без электрики)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Bosh (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Samsung (без электрики)", Price="1400"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Samsung (без электрики)", Price="700"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Siemens (без электрики)", Price="1300"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Siemens (без электрики)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Ariston (без электрики)", Price="1300"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Ariston (без электрики)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Electrolux (без электрики)", Price="1100"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Electrolux (без электрики)", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Gorenje (без электрики)", Price="1100"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Gorenje (без электрики)", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Установка посудомоечной машины Hansa (без электрики)", Price="1100"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины Hansa (без электрики)", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Установка встраиваемой посудомоечной машины (без электрики)", Price="1600"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж встраиваемой посудомоечной машины (без электрики)", Price="800"},
                    new CategoryInfo(){CategoryId=7, Name="Установка узкой посудомоечной машины (без электрики)", Price="1600"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж узкой посудомоечной машины (без электрики)", Price="800"},
                    new CategoryInfo(){CategoryId=7, Name="Демонтаж посудомоечной машины других брендов", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Герметизация стыков посудомоечной машины", Price="450"},
                    new CategoryInfo(){CategoryId=7, Name="Подключение слива посудомоечной машины (с герметизацией)", Price="650"},
                    new CategoryInfo(){CategoryId=7, Name="Штроба по кирпичу/бетону", Price="550"},
                    new CategoryInfo(){CategoryId=7, Name="Штроба по штукатурке и гипсу", Price="450"},
                    new CategoryInfo(){CategoryId=7, Name="Бурение отверстий под трубы", Price="750"},
                    new CategoryInfo(){CategoryId=7, Name="Разводка труб канализации (в зависимости от диаметра)", Price="750"},

                    new CategoryInfo(){CategoryId=8, Name="Разводка труб водоснабжения/отопления (металлопласт)", Price="950"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж труб водоснабжения/отопления (металлопласт)", Price="450"},
                    new CategoryInfo(){CategoryId=8, Name="Замена труб водоснабжения/отопления (металлопласт)", Price="1400"},
                    new CategoryInfo(){CategoryId=8, Name="Разводка труб водоснабжения/отопления (полипропилен)", Price="850"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж труб водоснабжения/отопления (полипропилен)", Price="350"},
                    new CategoryInfo(){CategoryId=8, Name="Замена труб водоснабжения/отопления (полипропилен)", Price="1200"},
                    new CategoryInfo(){CategoryId=8, Name="Разводка труб водоснабжения/отопления Рехау (Rehay)", Price="3300"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж труб водоснабжения/отопления Рехау (Rehay)", Price="1600"},
                    new CategoryInfo(){CategoryId=8, Name="Замена труб водоснабжения/отопления Рехау (Rehay)", Price="4900"},
                    new CategoryInfo(){CategoryId=8, Name="Разводка труб водоснабжения/отопления (медь)", Price="2300"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж труб водоснабжения/отопления (медь)", Price="1100"},
                    new CategoryInfo(){CategoryId=8, Name="Замена труб водоснабжения/отопления (медь)", Price="3400"},
                    new CategoryInfo(){CategoryId=8, Name="Разводка труб канализации (в зависимости от диаметра)", Price="750"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж труб канализации (в зависимости от диаметра)", Price="350"},
                    new CategoryInfo(){CategoryId=8, Name="Замена труб канализации (в зависимости от диаметра)", Price="1100"},
                    new CategoryInfo(){CategoryId=8, Name="Соединение канализационной трубы со стояком", Price="650"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фановой трубы лежак D=50мм", Price="650"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фановой трубы лежак D=50мм", Price="250"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фановой трубы лежак D=50мм", Price="900"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фановой трубы лежак D=100мм", Price="750"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фановой трубы лежак D=100мм", Price="350"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фановой трубы лежак D=100мм", Price="1100"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фановой трубы лежак D=110мм", Price="850"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фановой трубы лежак D=110мм", Price="450"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фановой трубы лежак D=110мм", Price="1300"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фановой трубы лежак D=150мм", Price="950"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фановой трубы лежак D=150мм", Price="550"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фановой трубы лежак D=150мм", Price="1500"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фановой трубы стояк D=110мм", Price="1250"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фановой трубы стояк D=110мм", Price="650"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фановой трубы стояк D=110мм", Price="1900"},
                    new CategoryInfo(){CategoryId=8, Name="Установка фанового тройника D=110мм", Price="1150"},
                    new CategoryInfo(){CategoryId=8, Name="Демонтаж фанового тройника D=110", Price="550"},
                    new CategoryInfo(){CategoryId=8, Name="Замена фанового тройника D=110", Price="1700"},
                    new CategoryInfo(){CategoryId=8, Name="Нарезка резьбы (D=1/2)", Price="500"},
                    new CategoryInfo(){CategoryId=8, Name="Нарезка резьбы (D=3/4)", Price="600"},
                    new CategoryInfo(){CategoryId=8, Name="Нарезка резьбы (D=1 и более)", Price="700"},
                    new CategoryInfo(){CategoryId=8, Name="Установка отсекающего вентиля (демонтаж/монтаж)", Price="700"},

                    new CategoryInfo(){CategoryId=9, Name="установка и замена радиатора", Price="2000"}
                };

                categoriesInfo.ForEach(x =>
                {
                    if (!x.Price.Any(y => char.IsLetter(y)))
                    {
                        x.Price = "от " + x.Price + " ₽";
                    }
                });

                context.CategoriesInfo.AddRange(categoriesInfo);
            }

            context.SaveChanges();
        }
    }
}
