using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Models
{
    public enum RequestStatus
    {
        New,
        Processed,
        Closed
    }
}
