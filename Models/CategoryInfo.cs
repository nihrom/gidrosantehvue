using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Models
{
    public class CategoryInfo
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
    }
}
