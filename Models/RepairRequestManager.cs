using GidrosantehVue.Services;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Models
{
    public class RepairRequestManager
    {
        ApplicationDbContext dbContext;
        IEmailService emailService;
        IBackgroundJobClient backgroundJobClient;

        public RepairRequestManager(ApplicationDbContext dbContext, IEmailService emailService, IBackgroundJobClient backgroundJobClient)
        {
            this.dbContext = dbContext;
            this.emailService = emailService;
            this.backgroundJobClient = backgroundJobClient;
        }

        public void CreateRequest(RepairRequest repairRequest)
        {
            repairRequest.DateCreation = DateTime.UtcNow;
            repairRequest.Id = 21;
            dbContext.RepairRequests.Add(repairRequest);
            dbContext.SaveChanges();
            backgroundJobClient.Enqueue(()=> ProcessRequestAsync(repairRequest.Id));
        }

        public async Task ProcessRequestAsync(int id)
        {
            var request = dbContext.RepairRequests.Find(id);
            if(request.RequestStatus == RequestStatus.New)
            {
                await emailService.SendRequestForRepair(request);
                request.RequestStatus = RequestStatus.Processed;
                dbContext.SaveChanges();
            }
        }
    }
}
