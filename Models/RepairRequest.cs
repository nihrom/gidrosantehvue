using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Models
{
    public class RepairRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public RequestStatus RequestStatus { get; set; }
        public DateTime DateCreation { get; set; }
        public DateTime? DateClosing { get; set; }
    }
}
