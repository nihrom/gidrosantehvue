﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GidrosantehVue.Migrations
{
    public partial class RepairRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateClosing",
                table: "RepairRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreation",
                table: "RepairRequests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateClosing",
                table: "RepairRequests");

            migrationBuilder.DropColumn(
                name: "DateCreation",
                table: "RepairRequests");
        }
    }
}
