import HomePage from 'components/home-page'
import CategoryInfo from 'components/category-info'

export const routes = [
  { name: 'home', path: '/', component: HomePage, display: 'Home', icon: 'home' },
  { name: 'category', path: '/category/:id', component: CategoryInfo, display: 'About Template', icon: 'info' },
]
