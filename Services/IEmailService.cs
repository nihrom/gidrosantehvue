﻿using GidrosantehVue.Models;
using System.Threading.Tasks;

namespace GidrosantehVue.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
        Task SendRequestForRepair(RepairRequest request);
    }
}