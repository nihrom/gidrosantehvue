using GidrosantehVue.Models;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidrosantehVue.Services
{
    public class EmailService : IEmailService
    {
        string emailTo;

        public EmailService(string emailTo)
        {
            this.emailTo = emailTo;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Gidrasancenter", "postmaster@gidrosancenter.ru"));
            emailMessage.To.Add(new MailboxAddress(email, email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("mail.gidrosancenter.ru", 587, false);
                await client.AuthenticateAsync("postmaster@gidrosancenter.ru", "ZjKYA5&pQR3R");//"5N9uJ42GrZqC");//"kV3J$qwEFMEjwr#z");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }

        public async Task SendRequestForRepair(RepairRequest request)
        {
            await SendEmailAsync(emailTo, "Заявка с Гидросанцентра", "Телефон - " + request.Phone + " Имя - " + request.Name);
        }
    }
}
